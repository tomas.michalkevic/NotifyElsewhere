package com.tomasmichalkevic.notifyelsewhere;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v4.*;
import android.support.v7.*;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import static android.app.PendingIntent.getActivity;

/**
 * Created by TomasMichalkevic on 08/06/15.
 */
public class SettingsActivity extends PreferenceActivity{

    Context context;
    Intent sms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);
        context = this;
        sms = new Intent(this, SMSHandler.class);
        final AlertDialog.Builder sendSMSDialog = new AlertDialog.Builder(this);
        Log.i("Settings", "In OnCreate");
        Preference smsPopup = (Preference) findPreference("sendTestSMS");
        smsPopup.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                sendSMSDialog.setTitle("Send Test SMS Message");
                sendSMSDialog.setMessage("Please confirm that you want to send a test message to the specified mobile number. Please note that standard network charges will apply.");
                AlertDialog.Builder builder = sendSMSDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Send the test message
                        //new SMSHandler(getNumber(), "Test", context).sendMessage();
                        Log.i("Settings", "Clicked to send SMS");
                        if(getNumber().equals("")){
                            Context context = getApplicationContext();
                            CharSequence text = "No target number was provided";
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                        }else{
                            sms.putExtra("number", getNumber());
                            sms.putExtra("testText", "If you have received this message, number was configured properly.");
                            startActivityForResult(sms, 1);
                        }
                        //boolean messageSent = true; //Get this status from the SMS handler(to be created). This one is for testing only.
                        /**if(messageSent){//If message was sent successfully then show a successful toast
                         Context context = getApplicationContext();
                         CharSequence text = "Message Sent";
                         int duration = Toast.LENGTH_SHORT;
                         Toast toast = Toast.makeText(context, text, duration);
                         toast.show();
                         }else{//Show a negative toast if unsuccessful message attempt
                         Context context = getApplicationContext();
                         CharSequence text = "Message sending failed";
                         int duration = Toast.LENGTH_SHORT;
                         Toast toast = Toast.makeText(context, text, duration);
                         toast.show();

                         }*/
                    }
                });
                sendSMSDialog.setNegativeButton("Do not send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do nothing if clicked "Do not send"
                        getNumber();
                    }
                });
                AlertDialog dialog = sendSMSDialog.create();
                dialog.show();
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    private String getNumber(){
        Preference targetDevNumber = (Preference) findPreference("targetDevNumber");
        String number = targetDevNumber.getSharedPreferences().getString("targetDevNumber", "No number");
        Log.i("Settings", "Got the number and it is: " + number);
        return number;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 1){
            Context context = getApplicationContext();
            CharSequence text = "Message Sent";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }else{
            Context context = getApplicationContext();
            CharSequence text = "Message Sending Failed";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}
