package com.tomasmichalkevic.notifyelsewhere;

import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;
import android.os.Bundle;

/**
 * Created by TomasMichalkevic on 11/06/15.
 */
public class SMSHandler extends Activity{

    private Context context;
    private BroadcastReceiver smsSentReceiver, smsDeliveredReceiver;
    private String status;
    private Intent returnResult;
    private Bundle extrasFromCallingIntent;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Log.i("SMSHandler", "In OnCreate");
        returnResult = new Intent();
        context = this;
        extrasFromCallingIntent = getIntent().getExtras();
        if(extrasFromCallingIntent!=null){
            SmsManager manager = SmsManager.getDefault();
            PendingIntent piSent=PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT"), 0);
            PendingIntent piDelivered= PendingIntent.getBroadcast(context, 0, new Intent("SMS_DELIVERED"), 0);
            manager.sendTextMessage(extrasFromCallingIntent.getString("number"), null, extrasFromCallingIntent.getString("testText"), piSent, piDelivered);
            Log.i("In SMSHandler", "Message sent");
            setResult(1, returnResult);
        }else{
            Log.i("In SMSHandler", "No extras were received");
            setResult(0, returnResult);
        }
        finish();
    }

    public SMSHandler(){
    }

    public void onResume(){
        super.onResume();
        smsSentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                 /**   case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS has been sent", Toast.LENGTH_SHORT).show();
                        status = "SMS has been sent";
                        Log.i("In SMSHandler Sent", "Now in SMS has been sent");
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic Failure", Toast.LENGTH_SHORT).show();
                        status = "Generic Failure";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No Service", Toast.LENGTH_SHORT).show();
                        status = "No Service";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                        status = "Null PDU";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio Off", Toast.LENGTH_SHORT).show();
                        status = "Radio Off";
                        break;**/
                    default:
                        break;
                }
            }
        };
        smsDeliveredReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
               /** switch(getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS Delivered", Toast.LENGTH_SHORT).show();
                        status = "SMS Delivered";
                        Log.i("In SMSHandler Delivered", "Successful");
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                        status = "SMS not delivered";
                        break;
                }**/
            }
        };
        registerReceiver(smsSentReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(smsDeliveredReceiver, new IntentFilter("SMS_DELIVERED"));
    }

    public void onPause() {
        super.onPause();
        unregisterReceiver(smsSentReceiver);
        unregisterReceiver(smsDeliveredReceiver);
    }
}
